FROM python:3.7

COPY . /app/
WORKDIR /app

RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED 1
ENV PROCESSES 4

ENTRYPOINT python __main__.py
